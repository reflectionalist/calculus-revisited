## Differentiation/Derivation Rules

### Lagrange's Notation for Derivatives

For $y = f(x)$, if $$\frac{\mathrm{d}y}{\mathrm{d}x}$$ (itself a function of $x$) is alternatively notated $f'(x)$, then
$$
\mathrm{d}y = f'(x)\mathrm{d}x \textsf{.}
$$

### The Constant Factor Rule

Find the differential and thence the derivative of $y = a f(x)​$, where $a​$ is a constant.

### The Sum Rule

Find the differential and thence the derivative of $y = f(x) + g(x)$.

### The Product Rule

Find the differential and thence the derivative of $y = f(x) \cdot g(x)$.

### The Quotient Rule

Find the differential and thence the derivative of $$y = \frac{f(x)}{g(x)}$$.

### The Chain Rule

Find the differential and thence the derivative of $y = g(f(x))​$.