# Calculus _à la_ Leibniz and Newton

|              DAI (Hong-)Yi               |
| :--------------------------------------: |
| [![Lecture Notes on CIE IAL Mathematics by DAI (Hong-)Yi is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/) |



![](images/quoteLeibniz.png)

![](images/quoteNewton.png)

## On Different Orders of Smallness

Put proper comparison operators in between
$$
1\ \textsf{hour}\ \_\_\_\_\ 1\ \textsf{minute}\ \_\_\_\_\ 1\ \textsf{second}
$$

> So, Nat'ralists observe, a Flea  
> Hath smaller Fleas that on him prey.  
> And these have smaller Fleas to bite'em,  
> And so proceed _ad infinitum_.
>
> --- Jonathan Swift, _On Poetry: A Rhapsody_ (1733)

![](images/theSizeOfAFlea.jpg)

Put proper comparison operators in between
$$
\textsf{Nat'ralists}\ \_\_\_\_\ \textsf{a Flea}\ \_\_\_\_\ \textsf{Fleas that on him prey}\ \_\_\_\_\ \textsf{Fleas to bite'em}
$$
What can we conclude from the two comparisons of smallness?

## "The Ghosts of Departed Quantities"[^Berkeley]

> A ghost is haunting Calculus --- the ghost of **Infinitesimal**.[^borrowing]

An infinitesimal _change_ or _difference_ of $\_$, notated
$$
\mathrm{d}\_ \textsf{,}
$$
is called the **differential** of $\_$, meaning "_little_ (change/difference of) $\_$", _e.g._, $\mathrm{d}x$, $\mathrm{d}y$, _etc_.

If $\mathrm{d}x$ is infinitesimal, what about $(\mathrm{d}x)^2$, $(\mathrm{d}x)^3$, _etc._?

## From Differential to Derivative

For a function $y = f(x)$,
$$
\frac{\mathrm{d}y}{\mathrm{d}x} \textsf{,}
$$
representing
$$
\textsf{the rate of change of $y$ with respect to $x$,}
$$
is called the **derivative** of $y$ _w.r.t._ $x$.

###### Example

Find the differential $\mathrm{d}y$ and thence the derivative $$\frac{\mathrm{d}y}{\mathrm{d}x}$$ of $y = x^2$.

###### Exercise

Find the differential and thence the derivative of the following functions:

(a)	$y = c$;

(b)	$y = x$;

(c)	$y = x^3$.
## Differentiating/Deriving Power Functions

Show that for power function $y = x^n$,
$$
\frac{\mathrm{d}y}{\mathrm{d}x} = nx^{n-1} \text{.}
$$
To what extent have you proved the differentiation/derivation formula for $y = x^{n}$?  Why?

## Notes

[^Berkeley]: Berkeley in _The Analyst_ [1734] questioned infinitesimals used by Newton, "And what are these Fluxions?  The Velocities of evanescent Increments?  And what are these same evanescent Increments?  They are neither finite Quantities nor Quantities infinitely small, nor yet nothing.  May we not call them the ghosts of departed quantities?"

[^borrowing]: Borrowing the proclamation of _Manifesto of the Communist Party_ [Marx and Engels 1848]: "A spectre is haunting Europe --- the spectre of Communism."

