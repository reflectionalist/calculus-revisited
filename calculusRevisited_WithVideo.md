# Calculus^®^ upon Giants![](images/supermanRaised.png)

|              DAI (Hong-)Yi               |
| :--------------------------------------: |
| [![Lecture Notes on CIE IAL Mathematics by DAI (Hong-)Yi is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](images/licenseButton.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/) |

|                            |                                  |                                      | ![](images/Cauchy+Bolzano+Weierstrauss+Riemann.jpg) |
| -------------------------- | -------------------------------- | ------------------------------------ | ---------------------------------------- |
|                            |                                  | ![](images/Newton+Leibniz+Euler.jpg) |                                          |
|                            | ![](images/Descartes+Fermat.jpg) |                                      |                                          |
| ![](images/Archimedes.jpg) |                                  |                                      |                                          |

<img src="images/quoteArchimedes.png" style="margin:0px auto; display:block">

<img src="images/missionImpossible.png" style="margin:0px auto; display:block">

<img src="images/missionPossible.jpg" style="margin:0px auto; display:block">

<video src="videos/missionStatement.ogg" style="margin:0px auto; width: 90%; display:block" onclick="this.play();"></video>

|                  Group                   | ![](images/noImageAvailable.png)  bar-Hiyya[^bar-Hiyya] | ![da-Vinci](images/da-Vinci.png)  da-Vinci[^da-Vinci] | ![](images/Riemann.jpg)  Riemann |
| :--------------------------------------: | :--------------------------------------: | :--------------------------------------: | :------------------------------: |
| **Pre-calculus: _Divide and (Reassemble)_** |                                          |                                          |                                  |
| **Calculus: _Differentiate and Integrate_** |                                          |                                          |                                  |

## $\sum$mary

<img src="images/circlesFurtherToSpheres.png" style="margin:0px auto; display:block">

## References

[^bar-Hiyya]: Alexander Bogomolny.   [Area of a Circle by Rabbi Abraham bar Hiyya Hanasi](http://www.cut-the-knot.org/Curriculum/Geometry/RABH.shtml).   Interactive Mathematics Miscellany and Puzzles, Accessed 2 June 2017.

[^da-Vinci]: Alexander Bogomolny.  [Area of a Circle by Leonardo da Vinci](http://www.cut-the-knot.org/Curriculum/Geometry/AreaOfCircle.shtml).  Interactive Mathematics Miscellany and Puzzles, Accessed 2 June 2017.

