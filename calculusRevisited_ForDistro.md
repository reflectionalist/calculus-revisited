# Calculus^®^ upon Giants![](images/supermanRaised.png)

|              DAI (Hong-)Yi               |
| :--------------------------------------: |
| [Lecture Notes on CIE IAL Mathematics by DAI (Hong-)Yi is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/) |

## Super Giants

|                |                       |                            | Cauchy, Bolzano, Weierstrauss, Riemann |
| -------------- | --------------------- | -------------------------- | -------------------------------------- |
|                |                       | **Newton, Leibniz, Euler** |                                        |
|                | **Descartes, Fermat** |                            |                                        |
| **Archimedes** |                       |                            |                                        |

| _Do not disturb my circle!_ |                    |
| --------------------------: | :----------------- |
|                             | **--- Archimedes** |

## Mission: ~~Im~~possible

## Mission Statement

> **Derive the area formula $A = \pi R^2$ for a circle of radius $R$.**

## Mission Execution

|                  Group                   | bar-Hiyya[^bar-Hiyya] | da-Vinci[^da-Vinci] | Riemann |
| :--------------------------------------: | :-------------------: | :-----------------: | :-----: |
| **Pre-calculus: _Divide and (Reassemble)_** |                       |                     |         |
| **Calculus: _Differentiate and Integrate_** |                       |                     |         |

## $\sum$mary

| circles | $\xrightarrow{\textsf{KEEP CALM AND GO FURTHER}}$ | spheres |
| ------: | :--------------------------------------: | :------ |
|         |                                          |         |

## References

[^bar-Hiyya]: Alexander Bogomolny.   [Area of a Circle by Rabbi Abraham bar Hiyya Hanasi](http://www.cut-the-knot.org/Curriculum/Geometry/RABH.shtml).   Interactive Mathematics Miscellany and Puzzles, Accessed 2 June 2017.
[^da-Vinci]: Alexander Bogomolny.  [Area of a Circle by Leonardo da Vinci](http://www.cut-the-knot.org/Curriculum/Geometry/AreaOfCircle.shtml).  Interactive Mathematics Miscellany and Puzzles, Accessed 2 June 2017.

