# Calculus Revisited upon Giants' Shoulders

## Introduction

Both of my two demo lessons, one department-wide and one school-wide, are about calculus, which is a difficult topic for high-school students.  The difficulty lies more in conceptions than in computations.  Many students can handle computations, symbolic or numeric, by following steps or using calculators, after a certain amount of practices.  Problem is, not many of them really understand the idea behind calculus.  What I keep telling students is this: if they know what only, they are hard disks; if they know how, better machines; if they know why, human.  My goal thus is to guide them to comprehend the essence of calculus.  There is no better source to grasp the essential idea of calculus than from its inventors and contributors.

## The First Demo

At the time of the first demo, students have already learned how to differentiate and integrate polynomial functions.  However, their understanding of the two key operations of calculus, namely differentiation and integration, was so fragile that they had no idea of what they are doing or so superficial that they had no grasp of the deep meaning behind the two processes.  In the lesson, I tried to reintroduce calculus, following not the derivative-first norm, but a differential-first approach, in the style of Leibniz and Newton.  The lesson, as far as I surveyed afterwards, to quite an extent, revealed the mystery of differential.  And a subsequent lesson dealt the same with integral.  However, without the demystification of differential in the demo, the illustration of integral could not follow through.

<img src="images/calculusALaLeibnizAndNewton.png" style="margin: 0px auto; display: block;">

## The Second Demo

<img src="images/calculusRevisitedUponGiants'Shoulders.png" style="margin: 0px auto; display: block;">

## Conclusion

